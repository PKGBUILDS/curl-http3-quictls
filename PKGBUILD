# Maintainer: ObserverOfTime <chronobserver@disroot.org>
# based on curl-http3-msquic

pkgname=curl-http3-nghttp3
pkgver=8.6.0
pkgrel=1
pkgdesc="A URL retrieval utility and library - compiled with HTTP/3 support"
arch=('x86_64')
url="https://curl.se"
license=('MIT')
depends=('brotli' 'c-ares' 'ca-certificates' 'krb5'
         'libidn2' 'libnghttp3' 'libpsl' 'libssh2'
         'openssl>=3.2' 'zlib' 'zstd')
makedepends=('git' 'perl')
conflicts=('curl' 'curl-http3')
provides=("curl=${pkgver%.r*}" "curl-http3=${pkgver%.r*}" 'libcurl.so=4-64')
source=("${url}/download/curl-${pkgver}.tar.bz2")
sha256sums=('SKIP')
options=('staticlibs')

build() {
  mkdir -p build-curl

  cd "${srcdir}/curl-${pkgver}"
  autoreconf -fi

  cd "${srcdir}/build-curl"
  "${srcdir}/curl-${pkgver}"/configure \
    --prefix=/usr \
    --mandir=/usr/share/man \
    --with-libssh2 \
    --with-gssapi \
    --with-nghttp3 \
    --with-openssl \
    --with-openssl-quic \
    --with-random=/dev/urandom \
    --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt \
    --enable-ipv6 \
    --enable-ares \
    --enable-alt-svc \
    --enable-threaded-resolver \
    --enable-versioned-symbols \
    --disable-ldap \
    --disable-ldaps \
    --disable-manual
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make
}

package() {
  cd build-curl
  make DESTDIR="${pkgdir}" install
  make DESTDIR="${pkgdir}" install -C scripts

  cd "${srcdir}/curl-${pkgver}"
  install -Dt "${pkgdir}/usr/share/licenses/${pkgname}" -m0644 COPYING
}
